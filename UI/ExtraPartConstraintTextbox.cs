﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ExtraPartsPositioning
{
  public enum CoordinateType
  {
    X,
    Y,
    Z
  }

  public class ExtraPartConstraintTextbox : TextBox
  {
    public static readonly DependencyProperty RefedExtraPartTypeProperty =
      DependencyProperty.Register("RefedExtraPartType", typeof(Model.ExtraPartType), typeof(ExtraPartConstraintTextbox), new PropertyMetadata(Model.ExtraPartType.FrontBumper));
    public static readonly DependencyProperty RefedCoordinateProperty =
      DependencyProperty.Register("RefedCoordinate", typeof(CoordinateType), typeof(ExtraPartConstraintTextbox), new PropertyMetadata(CoordinateType.X));

    public Model.ExtraPartType RefedExtraPartType
    {
      get
      {
        return (Model.ExtraPartType)GetValue(RefedExtraPartTypeProperty);
      }
      set
      {
        SetValue(RefedExtraPartTypeProperty, value);
      }
    }
    public CoordinateType RefedCoordinate
    {
      get
      {
        return (CoordinateType)GetValue(RefedCoordinateProperty);
      }
      set
      {
        SetValue(RefedCoordinateProperty, value);
      }
    }
  }
}
