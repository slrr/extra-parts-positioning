﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using System.Windows.Media.Animation;
using SlrrLib.View;

namespace ExtraPartsPositioning
{
  internal enum LoadMethod
  {
    NotLoaded,
    Normal,
    OnlyStockParts,
    HandleScriptChange
  }

  public partial class MainWindow : Window
  {
    private const float tickForMove = 0.01f;

    private LoadMethod currentLoadMethod;
    private Model.ExtraPartManager extraParts = new Model.ExtraPartManager();
    private SlrrLib.Geom.CarRpkModelFactory currentFactory = null;
    private Dictionary<string, string> whiteListSuperClassToRpkScriptRef;
    private SlrrLib.Model.DynamicRpk rpkForScriptChange = null;
    private string rpkFnam = "";
    private string highlightTexture = "grid.png";
    private string lastOpenFileDialogDir = System.IO.Directory.GetCurrentDirectory();

    private string ShortNameForCurrentData
    {
      get
      {
        return System.IO.Path.GetFileNameWithoutExtension(rpkFnam) + "_" + currentFactory.LastSelectedKey + "_0x" + currentFactory.LastSelectedIndex.ToString("X8");
      }
    }

    public MainWindow()
    {
      currentLoadMethod = LoadMethod.NotLoaded;
      InitializeComponent();
      if (System.IO.File.Exists("lastDir"))
        lastOpenFileDialogDir = System.IO.File.ReadAllText("lastDir");
      SlrrLib.Model.MessageLog.SetConsoleLogOutput();
      var suffixes = extraParts.GetSuffixes().ToList();
      suffixes.Insert(0, "");
      ctrlComboExtraPartPreView.ItemsSource = null;
      ctrlComboExtraPartPreView.ItemsSource = suffixes;
      ctrlComboExtraPartPreView.SelectedIndex = 0;
      generateCatalogJava();
      whiteListSuperClassToRpkScriptRef = new Dictionary<string, string>
      {
        {"Wing","parts\\scripts\\bodypart\\Wing.class"},
        {"Bumper","parts\\scripts\\bodypart\\Bumper.class"},
        {"DecorativeBodyPart","parts\\scripts\\bodypart\\DecorativeBodyPart.class"},
        {"FrontDoor","parts\\scripts\\bodypart\\FrontDoor.class"},
        {"FrontSeat","parts\\scripts\\bodypart\\FrontSeat.class"},
        {"GrilleGuard","parts\\scripts\\bodypart\\GrilleGuard.class"},
        {"HatchDoor","parts\\scripts\\bodypart\\HatchDoor.class"},
        {"Hood","parts\\scripts\\bodypart\\Hood.class"},
        {"IndicatorLights","parts\\scripts\\bodypart\\IndicatorLights.class"},
        {"Mirror","parts\\scripts\\bodypart\\Mirror.class"},
        {"Neon","parts\\scripts\\bodypart\\Neon.class"},
        {"Pedal","parts\\scripts\\bodypart\\Pedal.class"},
        {"Quarterpanel","parts\\scripts\\bodypart\\Quarterpanel.class"},
        {"RearDoor","parts\\scripts\\bodypart\\RearDoor.class"},
        {"RearSeat","parts\\scripts\\bodypart\\RearSeat.class"},
        {"RollBar","parts\\scripts\\bodypart\\RollBar.class"},
        {"RollCage","parts\\scripts\\bodypart\\RollCage.class"},
        {"Sideskirt","parts\\scripts\\bodypart\\Sideskirt.class"},
        {"Splitter","parts\\scripts\\bodypart\\Splitter.class"},
        {"TargaTop","parts\\scripts\\bodypart\\TargaTop.class"},
        {"Taillights","parts\\scripts\\bodypart\\Taillights.class"},
        {"Trunk","parts\\scripts\\bodypart\\Trunk.class"},
        {"Window","parts\\scripts\\bodypart\\Window.class"},
        {"Windshield","parts\\scripts\\bodypart\\Windshield.class"},
        {"VinylMesh","parts\\scripts\\bodypart\\VinylMesh.class"},
        {"ExtraLights","parts\\scripts\\bodypart\\ExtraLights.class"}
      };
      ctrlComboBoxPartScript.ItemsSource = null;
      ctrlComboBoxPartScript.ItemsSource = whiteListSuperClassToRpkScriptRef.Keys;
      ctrlComboBoxPartScript.SelectedIndex = 0;
    }

    private void refreshPivots()
    {
      ctrlListOfPivots.Items.Clear();
      foreach (var model in ctrlOrbitingViewport.CurrentModels)
        ctrlListOfPivots.Items.Add(model);
    }
    private void loadSceneRpk(LoadMethod method)
    {
      Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
      {
        InitialDirectory = lastOpenFileDialogDir,
        FileName = "",
        DefaultExt = ".scx",
        Filter = "CarRPKs|*.rpk",
        FilterIndex = 1
      };

      var result = dlg.ShowDialog();

      rpkFnam = "";
      if (result == true)
      {
        rpkFnam = dlg.FileName;
        lastOpenFileDialogDir = System.IO.Path.GetDirectoryName(rpkFnam);
      }
      else
      {
        return;
      }
      rpkForScriptChange = null;
      Title = rpkFnam;
      if (System.IO.Path.GetExtension(rpkFnam).ToLower() == ".rpk")
      {
        currentLoadMethod = method;
        if (method == LoadMethod.Normal)
        {
          currentFactory = new SlrrLib.Geom.CarRpkModelFactory(rpkFnam, true, true, true);
          ctrlOrbitingViewport.RenderScene(currentFactory);
          ctrlComboBoxFlappableSlots.ItemsSource = null;
          ctrlComboBoxFlappableSlots.ItemsSource = currentFactory.SelectedVehicleCfg.Slots
              .Where(x => x.SlotFlapLine != null)
              .Select(x => new Model.CfgPosLineView(x.SlotFlapLine, x));
        }
        else if (method == LoadMethod.OnlyStockParts)
        {
          currentFactory = new SlrrLib.Geom.CarRpkModelFactory(rpkFnam, true, true, true, true);
          ctrlOrbitingViewport.RenderScene(currentFactory);
        }
        else if (method == LoadMethod.HandleScriptChange)
        {
          currentFactory = new SlrrLib.Geom.CarRpkModelFactory(rpkFnam, false, true, true, false, true);
          ctrlOrbitingViewport.RenderScene(currentFactory);
        }
      }
      ctrlOrbitingViewport.AddMarker();
      System.IO.File.WriteAllText("lastDir", lastOpenFileDialogDir);
      removeExtraPartsImportedFromScene();
      refreshPivots();
      ctrlListOfPivots.SelectedIndex = -1;
      extraParts = new Model.ExtraPartManager();
      extraParts.ReLoadModels();
      addExtraPartsToScene();
      manageHighlighting();
      ctrlOrbitingViewport.ExtractUVsForPaintableModels();
    }
    private void manageHighlighting()
    {
      ctrlOrbitingViewport.SetAllModelsToTexture(@"grid_gray.png", 0.5, 0.5, Colors.DarkCyan);
      ctrlOrbitingViewport.SetTextureForPaintableModels(highlightTexture, 1, 0.7, Colors.Gray);
      foreach (var pivotModel in extraParts.SnappedModels)
      {
        ctrlOrbitingViewport.SetModelToColor(pivotModel.Value.ObjectModel, Colors.SkyBlue, 1, 1, Colors.SkyBlue);
      }
      foreach (var pivotModel in extraParts.ExtraPartModels)
      {
        ctrlOrbitingViewport.SetModelToColor(pivotModel.Value, Colors.SkyBlue, 1, 1, Colors.SkyBlue);
      }
      if (ctrlListOfPivots.SelectedItem is SlrrLib.Geom.NamedModel selItem)
      {
        if (!ctrlOrbitingViewport.IsMarker(selItem))
        {
          ctrlOrbitingViewport.SetAllModelsToTexture(@"grid_gray.png", 0.5, 0.5, Colors.DarkCyan);
          ctrlOrbitingViewport.SetModelToColor(selItem, Colors.Red, 1, 1, Colors.Red);
        }
      }
    }
    private void updateScriptLineOfTypeID(SlrrLib.Model.DynamicRpk rpk, int typeID, string newScriptSlrrRelativeFnam,
                                          string slrrRootDir, bool tryRedefinitionsIfMainResEntryFails = false)
    {
      string realclsFnam = newScriptSlrrRelativeFnam.Trim().Replace("/", "\\").Trim('\\');
      var res = rpk.Entries.Where(x => x.TypeID == typeID).ToList();
      if (res.Count == 0)
      {
        return;
      }
      if (!tryRedefinitionsIfMainResEntryFails && res.Count != 1)
      {
        return;
      }
      foreach (var r in res)
      {
        if (r.RSD.InnerEntries.Count() != 1)
        {
          continue;
        }
        if (!(r.RSD.InnerEntries.First() is SlrrLib.Model.DynamicStringInnerEntry rsd))
        {
          continue;
        }
        var spl = rsd.StringData.Split(new string[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
        bool wasChange = false;
        for (int spl_i = 0; spl_i != spl.Length; ++spl_i)
        {
          if (spl[spl_i].ToLower().StartsWith("script"))
          {
            spl[spl_i] = "script   " + realclsFnam;
            wasChange = true;
          }
        }
        if (wasChange)
        {
          rsd.StringData = spl.Aggregate((x, y) => x + "\r\n" + y) + "\r\n";
        }
      }
    }
    private void addSnappedModelsToScene()
    {
      foreach (var model in extraParts.SnappedModels)
      {
        if (!ctrlOrbitingViewport.CurrentModels.Contains(model.Value.ObjectModel))
          ctrlOrbitingViewport.AddModelToScene(model.Value.ObjectModel);
      }
    }
    private void removeSnappedModelsFromScene()
    {
      foreach (var model in extraParts.SnappedModels)
      {
        ctrlOrbitingViewport.RemoveModelFromScene(model.Value.ObjectModel);
      }
    }
    private void addExtraPartsToScene()
    {
      foreach (var model in extraParts.ExtraPartModels)
      {
        if (!ctrlOrbitingViewport.CurrentModels.Contains(model.Value))
          ctrlOrbitingViewport.AddModelToScene(model.Value);
      }
    }
    private void removeExtraPartsFromScene()
    {
      foreach (var model in extraParts.ExtraPartModels)
      {
        ctrlOrbitingViewport.RemoveModelFromScene(model.Value);
      }
    }
    private void removeExtraPartsImportedFromScene()
    {
      foreach (var model in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedScxModel>())
      {
        if (model.ScxFnam.Contains("_ExtraParts"))
          ctrlOrbitingViewport.RemoveModelFromScene(model);
      }
    }
    private float textBoxMouseWheelExtraParts(TextBox ctrTextBox, MouseWheelEventArgs e)
    {
      if (e == null)
      {
        return UIUtil.ParseOrZero(ctrTextBox.Text);
      }
      e.Handled = true;
      float deltaMult = 1.0f;
      if (Keyboard.IsKeyDown(Key.LeftShift))
        deltaMult += 1.0f;
      if (Keyboard.IsKeyDown(Key.LeftCtrl))
        deltaMult += 10.0f;
      if (Keyboard.IsKeyDown(Key.LeftAlt))
        deltaMult += 100.0f;
      if (UIUtil.ParseOrFalse(ctrTextBox.Text, out float cur))
      {
        float change = Math.Sign(e.Delta) * tickForMove * deltaMult;
        cur += change;
        ctrTextBox.Text = cur.ToString("F8", System.Globalization.CultureInfo.InvariantCulture);
        return cur;
      }
      return 0;
    }
    private void handleValueChangeOfXOffsetElement()
    {
      float cur = UIUtil.ParseOrZero(ctrlSliderExtraPartXOffsetFromSymmetry.Text);
      extraParts.XOffsetFromSymmetry = cur;
      extraParts.EnforceConstraints();
      extraParts.UpdateSanps();
    }
    private void handleValueChangeOfExtraPartConstraintElement(ExtraPartConstraintTextbox senderBox)
    {
      float cur = UIUtil.ParseOrZero(senderBox.Text);
      switch (senderBox.RefedCoordinate)
      {
        case CoordinateType.X:
          extraParts.ExtraPartModels[senderBox.RefedExtraPartType].BodyLine.LineX = cur;
          break;
        case CoordinateType.Y:
          extraParts.ExtraPartModels[senderBox.RefedExtraPartType].BodyLine.LineY = cur;
          break;
        case CoordinateType.Z:
          extraParts.ExtraPartModels[senderBox.RefedExtraPartType].BodyLine.LineZ = cur;
          break;
      }
      extraParts.EnforceConstraints();
      extraParts.UpdateSanps();
    }
    private void saveCurrentConfig()
    {
      StringBuilder sb = new StringBuilder();
      sb.AppendLine(ctrlSliderExtraPartXOffsetFromSymmetry.Text);
      sb.AppendLine(ctrlSliderExtraPartFrontBumperY.Text);
      sb.AppendLine(ctrlSliderExtraPartFrontBumperZ.Text);
      sb.AppendLine(ctrlSliderExtraPartFrontBumperLeftX.Text);
      sb.AppendLine(ctrlSliderExtraPartQuarterFrontFrontLeftBottomX.Text);
      sb.AppendLine(ctrlSliderExtraPartQuarterFrontFrontLeftBottomZ.Text);
      sb.AppendLine(ctrlSliderExtraPartQuarterFrontFrontLeftTopY.Text);
      sb.AppendLine(ctrlSliderExtraPartQuarterFrontBackLeftTopZ.Text);
      sb.AppendLine(ctrlSliderExtraPartSkirtLeftZ.Text);
      sb.AppendLine(ctrlSliderExtraPartSkirtLeftX.Text);
      sb.AppendLine(ctrlSliderExtraPartQuarterBackFrontLeftBottomX.Text);
      sb.AppendLine(ctrlSliderExtraPartQuarterBackFrontLeftBottomZ.Text);
      sb.AppendLine(ctrlSliderExtraPartQuarterBackFrontLeftTopY.Text);
      sb.AppendLine(ctrlSliderExtraPartQuarterBackBackLeftTopZ.Text);
      sb.AppendLine(ctrlSliderExtraPartRearBumperLeftZ.Text);
      sb.AppendLine(ctrlSliderExtraPartRearBumperLeftX.Text);
      string defaultName = ShortNameForCurrentData + "_Config.excfg";
      var dlg = new Microsoft.Win32.SaveFileDialog
      {
        InitialDirectory = System.IO.Directory.GetCurrentDirectory(),
        FileName = defaultName,
        DefaultExt = ".excfg",
        Filter = "excfgs|*.excfg"
      };
      bool ctrPressed = Keyboard.IsKeyDown(Key.LeftCtrl);
      if (ctrPressed)
      {
        var result = dlg.ShowDialog();
        if (result == true)
        {
          System.IO.File.WriteAllText(dlg.FileName, sb.ToString());
        }
      }
      else
      {
        System.IO.File.WriteAllText(defaultName, sb.ToString());
      }
    }
    private void saveSlotEdits()
    {
      saveCurrentConfig();
      extraParts.SaveExtraParts(rpkFnam, ShortNameForCurrentData, currentFactory.LastSelectedIndex, true);
    }
    private void generateCatalogJava()
    {
      if (System.IO.File.Exists("MergedExtraPartPakcs.txt"))
        System.IO.File.Delete("MergedExtraPartPakcs.txt");
      System.IO.File.CreateText("MergedExtraPartPakcs.txt").Close();
      foreach (var fnam in System.IO.Directory.EnumerateFiles(".", "ExtraPartsCatalogEntries_*"))
      {
        var lns = System.IO.File.ReadAllLines(fnam);
        for (int ln_i = 0; ln_i != lns.Length; ++ln_i)
        {
          if (lns[ln_i].EndsWith("toad = new ExtraPartCatalogEntry();"))
          {
            var currentCarID = lns[ln_i + 1].Split(' ').Last();
            currentCarID = currentCarID.Substring(0, currentCarID.Length - 1);
            lns[ln_i] = "\t\tif(TargetCarID == " + currentCarID + ")\r\n\t\t{\r\n" + lns[ln_i];
          }
          if (lns[ln_i].EndsWith("ExtraPartsPacks.addElement(toad);"))
          {
            lns[ln_i] = lns[ln_i] + "\r\n\t\t}";
          }
        }
        System.IO.File.AppendAllLines("MergedExtraPartPakcs.txt", lns);
      }
    }

    private void ctrlListOfPivots_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      manageHighlighting();
    }
    private void ctrlButtonFlipNormals_Click(object sender, RoutedEventArgs e)
    {
      foreach (var pivotModel in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedScxModel>())
      {
        if (pivotModel == null)
          continue;
        if (pivotModel.Scxv3Source == null &&
            pivotModel.Scxv4Source == null)
          continue;
        var model3D = pivotModel.ModelGeom as GeometryModel3D;
        var meshGeom = model3D.Geometry as MeshGeometry3D;
        meshGeom.TriangleIndices = new Int32Collection(meshGeom.TriangleIndices.Reverse());
        meshGeom.Normals = new Vector3DCollection(meshGeom.Normals.Select(x => -x));
      }
    }
    private void ctrlListOfPivots_KeyUp(object sender, KeyEventArgs e)
    {
      if (e.Key == Key.Delete)
      {
        foreach (var selPivot in ctrlListOfPivots.SelectedItems.Cast<SlrrLib.Geom.NamedModel>().ToList())
        {
          if (ctrlListOfPivots.SelectedIndex >= 0 && ctrlListOfPivots.SelectedIndex < ctrlListOfPivots.Items.Count)
          {
            ctrlOrbitingViewport.RemoveModelFromScene(selPivot);
            ctrlListOfPivots.Items.Remove(selPivot);
          }
        }
      }
      if (e.Key == Key.Right)
      {
        if (ctrlListOfPivots.SelectedItem is SlrrLib.Geom.NamedCfgInducedModel selItem)
        {
          for (int i = ctrlListOfPivots.SelectedIndex; i < ctrlListOfPivots.Items.Count; ++i)
          {
            if (ctrlListOfPivots.Items[i] is SlrrLib.Geom.NamedCfgInducedModel curItem)
            {
              if (curItem.SourceTypeID != selItem.SourceTypeID)
              {
                ctrlListOfPivots.SelectedIndex = i;
                ctrlListOfPivots.ScrollIntoView(ctrlListOfPivots.Items[i]);
                e.Handled = true;
                break;
              }
            }
          }
        }
      }
    }
    private void ctrlButtonChooseTexture_Click(object sender, RoutedEventArgs e)
    {
      Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
      {
        InitialDirectory = System.IO.Directory.GetCurrentDirectory(),
        FileName = "",
        DefaultExt = ".png",
        Filter = "PNGs|*.png|BMPs|*.bmp|JPGs|*.jpg"
      };

      var result = dlg.ShowDialog();

      if (result == true)
      {
        highlightTexture = dlg.FileName;
        manageHighlighting();
      }
      else
      {
        return;
      }
    }
    private void ctrlSliderExtraPartXOffsetFromSymmetry_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      if (!(sender is TextBox senderBox))
        return;
      textBoxMouseWheelExtraParts(senderBox, e);
      handleValueChangeOfXOffsetElement();
    }
    private void ctrlSliderExtraPartXOffsetFromSymmetry_KeyUp(object sender, KeyEventArgs e)
    {
      handleValueChangeOfXOffsetElement();
    }
    private void ctrlSliderExtraPart_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      if (!(sender is ExtraPartConstraintTextbox senderBox))
        return;
      textBoxMouseWheelExtraParts(senderBox, e);
      handleValueChangeOfExtraPartConstraintElement(senderBox);
    }
    private void ctrlSliderExtraPart_KeyUp(object sender, KeyEventArgs e)
    {
      if (!(sender is ExtraPartConstraintTextbox senderBox))
        return;
      handleValueChangeOfExtraPartConstraintElement(senderBox);
    }
    private void ctrlButtonHideExtraParts_Click(object sender, RoutedEventArgs e)
    {
      removeExtraPartsFromScene();
      removeSnappedModelsFromScene();
    }
    private void ctrlButtonShowExtraParts_Click(object sender, RoutedEventArgs e)
    {
      if (extraParts.SnappedModels.Any())
      {
        removeExtraPartsFromScene();
        addSnappedModelsToScene();
      }
      else
      {
        removeSnappedModelsFromScene();
        addExtraPartsToScene();
      }
    }
    private void ctrlComboExtraPartPreView_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (e.AddedItems.Count != 0)
      {
        removeExtraPartsFromScene();
        removeSnappedModelsFromScene();
        extraParts.FillSnappedModelsWithSuffix(e.AddedItems[0] as string);
        extraParts.UpdateSanps();
        extraParts.EnforceConstraints();
        addSnappedModelsToScene();
        if (e.AddedItems[0] as string == "")
          addExtraPartsToScene();
        else
          removeExtraPartsFromScene();
        manageHighlighting();
      }
    }
    private void ctrlButtonSaveExtras_Click(object sender, RoutedEventArgs e)
    {
      saveSlotEdits();
      generateCatalogJava();
    }
    private void ctrlButtonSaveOnlySlotEdits_Click(object sender, RoutedEventArgs e)
    {
      saveSlotEdits();
    }
    private void ctrlButtonSaveConfig_Click(object sender, RoutedEventArgs e)
    {
      saveCurrentConfig();
    }
    private void ctrlButtonLoadConfig_Click(object sender, RoutedEventArgs e)
    {
      string defaultName = ShortNameForCurrentData + "_Config.excfg";
      Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
      {
        InitialDirectory = System.IO.Directory.GetCurrentDirectory(),
        FileName = defaultName,
        DefaultExt = ".excfg",
        Filter = "excfgs|*.excfg"
      };
      bool ctrPressed = Keyboard.IsKeyDown(Key.LeftCtrl);
      bool shoudLoad = false;
      if (ctrPressed)
      {
        var result = dlg.ShowDialog();
        if (result == true)
        {
          shoudLoad = true;
        }
      }
      else
      {
        shoudLoad = true;
      }
      if (shoudLoad && System.IO.File.Exists(dlg.FileName))
      {
        var lns = System.IO.File.ReadAllLines(dlg.FileName);
        if (lns.Length < 16)
        {
          SlrrLib.Model.MessageLog.AddError("Extra parts config file contains " + lns.Length.ToString() + " lines which should be at least 16");
          return;
        }
        ctrlSliderExtraPartXOffsetFromSymmetry.Text = lns[0];
        ctrlSliderExtraPartFrontBumperY.Text = lns[1];
        ctrlSliderExtraPartFrontBumperZ.Text = lns[2];
        ctrlSliderExtraPartFrontBumperLeftX.Text = lns[3];
        ctrlSliderExtraPartQuarterFrontFrontLeftBottomX.Text = lns[4];
        ctrlSliderExtraPartQuarterFrontFrontLeftBottomZ.Text = lns[5];
        ctrlSliderExtraPartQuarterFrontFrontLeftTopY.Text = lns[6];
        ctrlSliderExtraPartQuarterFrontBackLeftTopZ.Text = lns[7];
        ctrlSliderExtraPartSkirtLeftZ.Text = lns[8];
        ctrlSliderExtraPartSkirtLeftX.Text = lns[9];
        ctrlSliderExtraPartQuarterBackFrontLeftBottomX.Text = lns[10];
        ctrlSliderExtraPartQuarterBackFrontLeftBottomZ.Text = lns[11];
        ctrlSliderExtraPartQuarterBackFrontLeftTopY.Text = lns[12];
        ctrlSliderExtraPartQuarterBackBackLeftTopZ.Text = lns[13];
        ctrlSliderExtraPartRearBumperLeftZ.Text = lns[14];
        ctrlSliderExtraPartRearBumperLeftX.Text = lns[15];
        handleValueChangeOfXOffsetElement();
        handleValueChangeOfExtraPartConstraintElement(ctrlSliderExtraPartFrontBumperY);
        handleValueChangeOfExtraPartConstraintElement(ctrlSliderExtraPartFrontBumperZ);
        handleValueChangeOfExtraPartConstraintElement(ctrlSliderExtraPartFrontBumperLeftX);
        handleValueChangeOfExtraPartConstraintElement(ctrlSliderExtraPartQuarterFrontFrontLeftBottomX);
        handleValueChangeOfExtraPartConstraintElement(ctrlSliderExtraPartQuarterFrontFrontLeftBottomZ);
        handleValueChangeOfExtraPartConstraintElement(ctrlSliderExtraPartQuarterFrontFrontLeftTopY);
        handleValueChangeOfExtraPartConstraintElement(ctrlSliderExtraPartQuarterFrontBackLeftTopZ);
        handleValueChangeOfExtraPartConstraintElement(ctrlSliderExtraPartSkirtLeftZ);
        handleValueChangeOfExtraPartConstraintElement(ctrlSliderExtraPartSkirtLeftX);
        handleValueChangeOfExtraPartConstraintElement(ctrlSliderExtraPartQuarterBackFrontLeftBottomX);
        handleValueChangeOfExtraPartConstraintElement(ctrlSliderExtraPartQuarterBackFrontLeftBottomZ);
        handleValueChangeOfExtraPartConstraintElement(ctrlSliderExtraPartQuarterBackFrontLeftTopY);
        handleValueChangeOfExtraPartConstraintElement(ctrlSliderExtraPartQuarterBackBackLeftTopZ);
        handleValueChangeOfExtraPartConstraintElement(ctrlSliderExtraPartRearBumperLeftZ);
        handleValueChangeOfExtraPartConstraintElement(ctrlSliderExtraPartRearBumperLeftX);
      }
    }
    private void ctrlButtonGenerateAll_Click(object sender, RoutedEventArgs e)
    {
      extraParts = new Model.ExtraPartManager();
      extraParts.ReLoadModels();
      foreach (var rpkFnam in System.IO.Directory.EnumerateFiles(@"F:\Prog\Game\Slrr\cars\racers", "*.rpk"))
      {
        SlrrLib.Model.MessageLog.AddMessage(System.IO.Path.GetFileName(rpkFnam));
        string rpkShortName = System.IO.Path.GetFileNameWithoutExtension(rpkFnam);
        var rpk = new SlrrLib.Model.HighLevel.RpkManager(rpkFnam, SlrrLib.Model.HighLevel.GameFileManager.GetSLRRRoot(rpkFnam));
        foreach (var carID in rpk.CarDefTypeIDs())
        {
          var res = rpk.GetResEntry(carID);
          if (res.Alias.StartsWith("traffic"))
            continue;
          var cfg = rpk.GetCfgFromTypeID(carID);
          string key = System.IO.Path.GetFileNameWithoutExtension(cfg.CfgFileName);
          string carName = rpkShortName + "_" + key + "_0x" + carID.ToString("X8");
          string defaultName = rpkShortName + "_" + key + "_0x" + carID.ToString("X8") + "_Config.excfg";
          if (System.IO.File.Exists(defaultName))
          {
            extraParts.LoadConfig(defaultName);
            extraParts.SaveExtraParts(rpkFnam, carName, carID, false, true);
          }
        }
      }
      generateCatalogJava();
    }
    private void ctrlButtonLowerWeightsOfNonMain_Click(object sender, RoutedEventArgs e)
    {
      Vector3D centroid = new Vector3D();
      float weight = 0.0f;
      HashSet<int> typeIDs = new HashSet<int>();
      List<SlrrLib.Model.CfgPartPositionLine> wheelLines = new List<SlrrLib.Model.CfgPartPositionLine>();
      foreach (var pivotObj in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedCfgInducedModel>())
      {
        if (pivotObj.Cfg != null)
        {
          if (typeIDs.Contains(pivotObj.SourceTypeID))
            continue;
          typeIDs.Add(pivotObj.SourceTypeID);
          SlrrLib.Model.MessageLog.AddMessage(pivotObj.Name + " -> " + System.IO.Path.GetFileName(pivotObj.Cfg.CfgFileName) + " | 0x" + pivotObj.SourceTypeID.ToString("X8"));
          foreach (var physLine in pivotObj.Cfg.BodyLines)
          {
            weight += physLine.BodyWeight;
            centroid += physLine.BodyWeight * (pivotObj.Translate + new Vector3D(physLine.LineX * 100.0f, physLine.LineY * 100.0f, physLine.LineZ * 100.0f));
          }
          if (pivotObj.Cfg.FirstIndexOfNamedLine("chassis") == -1)
          {
            foreach (var physLine in pivotObj.Cfg.BodyLines)
            {
              physLine.BodyWeight = 0.01f;
            }
            foreach (var line in pivotObj.Cfg.LinesWithName("wing"))
            {
              line.Tokens[0].Value = "#wing";
            }
          }
          else
          {
            SlrrLib.Model.MessageLog.AddMessage("Chassis");
            foreach (var physLine in pivotObj.Cfg.BodyLines)
            {
              if (physLine.BodyModel.ToLower() == "sphere" && physLine.BodyWeight < 1.0f)
                continue;
              physLine.BodyWeight = 0.01f;
            }
            foreach (var line in pivotObj.Cfg.LinesWithPositionData)
            {
              if (line.LineName == "wheel")
                wheelLines.Add(line);
            }
          }
        }
      }
      centroid /= weight;
      SlrrLib.Model.MessageLog.AddMessage("Centroid was: " + centroid.ToString() + "  weight was: " + weight.ToString());
      float balance = UIUtil.ParseOrZero(ctrlTextBoxBalanceFR.Text);
      SlrrLib.Model.MessageLog.AddMessage("Adding weight for " + wheelLines.Count.ToString() + " wheels full weight will be " + weight.ToString() + " balance: " + balance.ToString());
      if (ctrlCheckBoxUseCalculatedBalance.IsChecked.Value)
      {
        SlrrLib.Model.MessageLog.AddMessage("Will be using the calculated balance: " + (centroid.Z / 100.0f).ToString());
        balance = (float)centroid.Z / 100.0f;
      }
      foreach (var wheel in wheelLines)
      {
        int toInsert = currentFactory.SelectedVehicleCfg.LastIndexOfNamedLine("body");
        toInsert--;
        currentFactory.SelectedVehicleCfg.InsertLine(toInsert, new SlrrLib.Model.CfgLine("nocollision"));
        currentFactory.SelectedVehicleCfg.InsertLine(toInsert, new SlrrLib.Model.CfgLine("noclick"));
        var bodyLine = new SlrrLib.Model.CfgBodyLine(new SlrrLib.Model.CfgLine("body    0.000 0.000 0.000 0.000 0.000 0.000 0.001 cars\\meshes\\BigfootRollPrevention\\phys_Box.SCX"));
        if (wheel.LineZ < 0)
          bodyLine.BodyWeight = (weight / (float)wheelLines.Count) * (1.0f - balance);
        else
          bodyLine.BodyWeight = (weight / (float)wheelLines.Count) * (1.0f + balance);
        bodyLine.LineX = wheel.LineX;
        bodyLine.LineY = wheel.LineY;
        bodyLine.LineZ = wheel.LineZ;
        currentFactory.SelectedVehicleCfg.InsertLine(toInsert, new SlrrLib.Model.CfgLine(bodyLine.ToString()));
      }
    }
    private void ctrlButtonSaveCfgs_Click(object sender, RoutedEventArgs e)
    {
      HashSet<int> typeIDs = new HashSet<int>();
      foreach (var pivotObj in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedCfgInducedModel>())
      {
        if (pivotObj.Cfg != null)
        {
          if (typeIDs.Contains(pivotObj.SourceTypeID))
            continue;
          typeIDs.Add(pivotObj.SourceTypeID);
          pivotObj.Cfg.Save();
        }
      }
    }
    private void ctrlButtonSetCurrentSlotSaveCFG_Click(object sender, RoutedEventArgs e)
    {
      if (ctrlComboBoxFlappableSlots.SelectedItem == null)
        return;
      var selectedFlapView = ctrlComboBoxFlappableSlots.SelectedItem as Model.CfgPosLineView;
      var selectedFlap = selectedFlapView.Line;
      if (selectedFlap == null)
        return;
      if (ctrlOrbitingViewport.IsMarkerSelected())
      {
        selectedFlap.LineX = (float)ctrlOrbitingViewport.MarkerPosition.X;
        selectedFlap.LineY = (float)ctrlOrbitingViewport.MarkerPosition.Y;
        selectedFlap.LineZ = (float)ctrlOrbitingViewport.MarkerPosition.Z;
        if (currentFactory == null)
          return;
        if (currentFactory.SelectedVehicleCfg == null)
          return;
        currentFactory.SelectedVehicleCfg.Save(true, "_BAK_CfgFlap_");
      }
    }
    private void ctrlButtonSaveRPK_Click(object sender, RoutedEventArgs e)
    {
      if (rpkForScriptChange == null)
        return;
      rpkForScriptChange.SaveAs(currentFactory.LastRPKMng.RpkFileName, true, "_BAK_MANUAL_SCRIPT_CHANGE_");
    }
    private void ctrlButtonSetNewScriptForPart_Click(object sender, RoutedEventArgs e)
    {
      if (ctrlListOfPivots.SelectedItem is SlrrLib.Geom.NamedCfgInducedModel selItem)
      {
        if (selItem == null)
          return;
        if (selItem.SourceTypeID == -1)
          return;
        if (currentFactory.LastRPKMng == null)
          return;
        if (rpkForScriptChange == null)
          rpkForScriptChange = new SlrrLib.Model.DynamicRpk(currentFactory.LastRPKMng.Rpk);
        if (!whiteListSuperClassToRpkScriptRef.ContainsKey(ctrlComboBoxPartScript.SelectedValue as string))
          return;
        updateScriptLineOfTypeID(rpkForScriptChange, selItem.SourceTypeID,
                                 whiteListSuperClassToRpkScriptRef[ctrlComboBoxPartScript.SelectedValue as string],
                                 currentFactory.LastRPKMng.SlrrRootDir);
        foreach (var pivotModel in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedCfgInducedModel>())
        {
          if (pivotModel == null)
            continue;
          if (pivotModel.SourceTypeID == selItem.SourceTypeID &&
              pivotModel.Cfg.CfgFileName == selItem.Cfg.CfgFileName &&
              pivotModel.ScxFnam == selItem.ScxFnam)
          {
            pivotModel.Name = ctrlComboBoxPartScript.SelectedValue + " | 0x" + pivotModel.SourceTypeID.ToString("X8");
          }
        }
        ctrlListOfPivots.InvalidateArrange();
        ctrlListOfPivots.UpdateLayout();
        ctrlListOfPivots.Items.Refresh();
      }
    }
    private void ctrlButtonRenderSceneNormal_Click(object sender, RoutedEventArgs e)
    {
      loadSceneRpk(LoadMethod.Normal);
    }
    private void ctrlButtonRenderSceneStock_Click(object sender, RoutedEventArgs e)
    {
      loadSceneRpk(LoadMethod.OnlyStockParts);
    }
    private void ctrlButtonRenderSceneScript_Click(object sender, RoutedEventArgs e)
    {
      loadSceneRpk(LoadMethod.HandleScriptChange);
    }
  }
}
