FN DetachMeshElements SourceOBJ =
	( 
		MasterObj = SourceOBJ
		meshOp.weldVertsByThreshold MasterObj.mesh MasterObj.mesh.verts 0.001
		turnMod = Turn_to_Mesh()
		addmodifier MasterObj turnMod 
		turnMod.useInvisibleEdges = off
		convertTo MasterObj (Editable_Poly) 
		faceCount = MasterObj.getNumFaces()
		
		FaceTest = ((MasterObj.getNumFaces()) != 0)
		firstObj = 1
		while FaceTest do
		(
			newName = (uniquename (MasterObj.name+"_" as string))
			if firstObj == 1 do
			(
				newName = "BaseObj"
				firstObj = 0
			)
			MasterObj.EditablePoly.SetSelection #Face #{1}
			MasterObj.selectElement()
			TargetElement = polyop.getFaceSelection MasterObj
			polyop.detachFaces MasterObj TargetElement asNode:true name:newName
			
			NewObj = getnodebyname newName
			convertTo NewObj (Editable_Mesh)
			FaceTest = ((MasterObj.getnumfaces()) != 0)
		)
		NewObj = getnodebyname "BaseObj"
		NewObj.name = MasterObj.name
		delete MasterObj
	)

max create mode
$.material = meditMaterials[3] --just if the paintable material is the 3rd in the material editor
ObjsSource = getCurrentSelection()
for obj in ObjsSource do
(
	if abs(obj.pos.x - 0.0)<0.1 and abs(obj.pos.y - 226.084)<0.1 and abs(obj.pos.z - -63.877)<0.1 do
	(
		obj.name = "FrontBumper"
	)
	if abs(obj.pos.x - -83.256)<0.1 and abs(obj.pos.y - 226.084)<0.1 and abs(obj.pos.z - -63.877)<0.1 do
	(
		obj.name = "FrontBumperLeft"
	)
	if abs(obj.pos.x - -83.256)<0.1 and abs(obj.pos.y - 172.717)<0.1 and abs(obj.pos.z - -63.877)<0.1 do
	(
		obj.name = "QuarterFrontFrontLeftBottom"
	)
	if abs(obj.pos.x - -83.256)<0.1 and abs(obj.pos.y - 172.717)<0.1 and abs(obj.pos.z - -27.577)<0.1 do
	(
		obj.name = "QuarterFrontFrontLeftTop"
	)
	if abs(obj.pos.x - -83.256)<0.1 and abs(obj.pos.y - 95.183)<0.1 and abs(obj.pos.z - -27.577)<0.1 do
	(
		obj.name = "QuarterFrontBackLeftTop"
	)
	if abs(obj.pos.x - -83.256)<0.1 and abs(obj.pos.y - 95.183)<0.1 and abs(obj.pos.z - -63.877)<0.1 do
	(
		obj.name = "QuarterFrontBackLeftBottom"
	)
	if abs(obj.pos.x - -83.256)<0.1 and abs(obj.pos.y - 6.535)<0.1 and abs(obj.pos.z - -63.877)<0.1 do
	(
		obj.name = "SkirtLeft"
	)
	if abs(obj.pos.x - -83.256)<0.1 and abs(obj.pos.y - -85.368)<0.1 and abs(obj.pos.z - -63.877)<0.1 do
	(
		obj.name = "QuarterBackFrontLeftBottom"
	)
	if abs(obj.pos.x - -83.256)<0.1 and abs(obj.pos.y - -85.368)<0.1 and abs(obj.pos.z - -27.577)<0.1 do
	(
		obj.name = "QuarterBackFrontLeftTop"
	)
	if abs(obj.pos.x - -83.256)<0.1 and abs(obj.pos.y - -162.676)<0.1 and abs(obj.pos.z - -27.577)<0.1 do
	(
		obj.name = "QuarterBackBackLeftTop"
	)
	if abs(obj.pos.x - -83.256)<0.1 and abs(obj.pos.y - -162.676)<0.1 and abs(obj.pos.z - -63.877)<0.1 do
	(
		obj.name = "QuarterBackBackLeftBottom"
	)
	if abs(obj.pos.x - -83.256)<0.1 and abs(obj.pos.y - -221.788)<0.1 and abs(obj.pos.z - -63.877)<0.1 do
	(
		obj.name = "RearBumperLeft"
	)
	if abs(obj.pos.x - 0.0)<0.1 and abs(obj.pos.y - -221.788)<0.1 and abs(obj.pos.z - -63.877)<0.1 do
	(
		obj.name = "RearBumper"
	)
	------
	if abs(obj.pos.x - 83.256)<0.1 and abs(obj.pos.y - 226.084)<0.1 and abs(obj.pos.z - -63.877)<0.1 do
	(
		obj.name = "FrontBumperRight"
	)
	if abs(obj.pos.x - 83.256)<0.1 and abs(obj.pos.y - 172.717)<0.1 and abs(obj.pos.z - -63.877)<0.1 do
	(
		obj.name = "QuarterFrontFrontRightBottom"
	)
	if abs(obj.pos.x - 83.256)<0.1 and abs(obj.pos.y - 172.717)<0.1 and abs(obj.pos.z - -27.577)<0.1 do
	(
		obj.name = "QuarterFrontFrontRightTop"
	)
	if abs(obj.pos.x - 83.256)<0.1 and abs(obj.pos.y - 95.183)<0.1 and abs(obj.pos.z - -27.577)<0.1 do
	(
		obj.name = "QuarterFrontBackRightTop"
	)
	if abs(obj.pos.x - 83.256)<0.1 and abs(obj.pos.y - 95.183)<0.1 and abs(obj.pos.z - -63.877)<0.1 do
	(
		obj.name = "QuarterFrontBackRightBottom"
	)
	if abs(obj.pos.x - 83.256)<0.1 and abs(obj.pos.y - 6.535)<0.1 and abs(obj.pos.z - -63.877)<0.1 do
	(
		obj.name = "SkirtRight"
	)
	if abs(obj.pos.x - 83.256)<0.1 and abs(obj.pos.y - -85.368)<0.1 and abs(obj.pos.z - -63.877)<0.1 do
	(
		obj.name = "QuarterBackFrontRightBottom"
	)
	if abs(obj.pos.x - 83.256)<0.1 and abs(obj.pos.y - -85.368)<0.1 and abs(obj.pos.z - -27.577)<0.1 do
	(
		obj.name = "QuarterBackFrontRightTop"
	)
	if abs(obj.pos.x - 83.256)<0.1 and abs(obj.pos.y - -162.676)<0.1 and abs(obj.pos.z - -27.577)<0.1 do
	(
		obj.name = "QuarterBackBackRightTop"
	)
	if abs(obj.pos.x - 83.256)<0.1 and abs(obj.pos.y - -162.676)<0.1 and abs(obj.pos.z - -63.877)<0.1 do
	(
		obj.name = "QuarterBackBackRightBottom"
	)
	if abs(obj.pos.x - 83.256)<0.1 and abs(obj.pos.y - -221.788)<0.1 and abs(obj.pos.z - -63.877)<0.1 do
	(
		obj.name = "RearBumperRight"
	)
	
	DetachMeshElements(obj)
)
max views redraw




