﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using System.Windows.Media;
using SlrrLib.Geom;

namespace ExtraPartsPositioning.Model
{
  public enum ExtraPartType
  {
    FrontBumper,// 0x00032006, slot 5000
    FrontBumperLeft,// 0x00032007, slot 5001
    QuarterFrontFrontLeftBottom,// 0x00032008, slot 5002
    QuarterFrontFrontLeftTop,// 0x00032009, slot 5003
    QuarterFrontBackLeftBottom,// 0x0003200A, slot 5004
    QuarterFrontBackLeftTop,// 0x0003200B, slot 5005
    SkirtLeft,// 0x0003200C, slot 5006
    QuarterBackFrontLeftBottom,// 0x0003200D, slot 5007
    QuarterBackFrontLeftTop,// 0x0003200E, slot 5008
    QuarterBackBackLeftBottom,// 0x0003200F, slot 5009
    QuarterBackBackLeftTop,// 0x00032010, slot 5010
    RearBumperLeft,// 0x00032011, slot 5011
    RearBumper,// 0x00032012, slot 5012
    FrontBumperRight,// 0x00032013, slot 5013
    QuarterFrontFrontRightBottom,// 0x00032014, slot 5014
    QuarterFrontFrontRightTop,// 0x00032015, slot 5015
    QuarterFrontBackRightBottom,// 0x00032016, slot 5016
    QuarterFrontBackRightTop,// 0x00032017, slot 5017
    SkirtRight,// 0x00032018, slot 5018
    QuarterBackFrontRightBottom,// 0x00032019, slot 5019
    QuarterBackFrontRightTop,// 0x0003201A, slot 5020
    QuarterBackBackRightBottom,// 0x0003201B, slot 5021
    QuarterBackBackRightTop,// 0x0003201C, slot 5022
    RearBumperRight// 0x0003201D, slot 5023
  }

  public class ExtraPartManager
  {
    private Dictionary<ExtraPartType, string> defaultFileName = new Dictionary<ExtraPartType, string>
    {
      { ExtraPartType.FrontBumper, "\\FrontBumper.scx" },
      { ExtraPartType.FrontBumperLeft, "\\FrontBumperLeft.scx" },
      { ExtraPartType.QuarterFrontFrontLeftBottom, "\\QuarterFrontFrontLeftBottom.scx" },
      { ExtraPartType.QuarterFrontFrontLeftTop, "\\QuarterFrontFrontLeftTop.scx" },
      { ExtraPartType.QuarterFrontBackLeftBottom, "\\QuarterFrontBackLeftBottom.scx" },
      { ExtraPartType.QuarterFrontBackLeftTop, "\\QuarterFrontBackLeftTop.scx" },
      { ExtraPartType.SkirtLeft, "\\SkirtLeft.scx" },
      { ExtraPartType.QuarterBackFrontLeftBottom, "\\QuarterBackFrontLeftBottom.scx" },
      { ExtraPartType.QuarterBackFrontLeftTop, "\\QuarterBackFrontLeftTop.scx" },
      { ExtraPartType.QuarterBackBackLeftBottom, "\\QuarterBackBackLeftBottom.scx" },
      { ExtraPartType.QuarterBackBackLeftTop, "\\QuarterBackBackLeftTop.scx" },
      { ExtraPartType.RearBumperLeft, "\\RearBumperLeft.scx" },
      { ExtraPartType.RearBumper, "\\RearBumper.scx" },
      { ExtraPartType.FrontBumperRight, "\\FrontBumperRight.scx" },
      { ExtraPartType.QuarterFrontFrontRightBottom, "\\QuarterFrontFrontRightBottom.scx" },
      { ExtraPartType.QuarterFrontFrontRightTop, "\\QuarterFrontFrontRightTop.scx" },
      { ExtraPartType.QuarterFrontBackRightBottom, "\\QuarterFrontBackRightBottom.scx" },
      { ExtraPartType.QuarterFrontBackRightTop, "\\QuarterFrontBackRightTop.scx" },
      { ExtraPartType.SkirtRight, "\\SkirtRight.scx" },
      { ExtraPartType.QuarterBackFrontRightBottom, "\\QuarterBackFrontRightBottom.scx" },
      { ExtraPartType.QuarterBackFrontRightTop, "\\QuarterBackFrontRightTop.scx" },
      { ExtraPartType.QuarterBackBackRightBottom, "\\QuarterBackBackRightBottom.scx" },
      { ExtraPartType.QuarterBackBackRightTop, "\\QuarterBackBackRightTop.scx" },
      { ExtraPartType.RearBumperRight, "\\RearBumperRight.scx" }
    };
    private Dictionary<int, ExtraPartType> slotIDToExtraPartType = new Dictionary<int, ExtraPartType>
    {
      {5000,ExtraPartType.FrontBumper},
      {5001,ExtraPartType.FrontBumperLeft},
      {5002,ExtraPartType.QuarterFrontFrontLeftBottom},
      {5003,ExtraPartType.QuarterFrontFrontLeftTop},
      {5004,ExtraPartType.QuarterFrontBackLeftBottom},
      {5005,ExtraPartType.QuarterFrontBackLeftTop},
      {5006,ExtraPartType.SkirtLeft},
      {5007,ExtraPartType.QuarterBackFrontLeftBottom},
      {5008,ExtraPartType.QuarterBackFrontLeftTop},
      {5009,ExtraPartType.QuarterBackBackLeftBottom},
      {5010,ExtraPartType.QuarterBackBackLeftTop},
      {5011,ExtraPartType.RearBumperLeft},
      {5012,ExtraPartType.RearBumper},
      {5013,ExtraPartType.FrontBumperRight},
      {5014,ExtraPartType.QuarterFrontFrontRightBottom},
      {5015,ExtraPartType.QuarterFrontFrontRightTop},
      {5016,ExtraPartType.QuarterFrontBackRightBottom},
      {5017,ExtraPartType.QuarterFrontBackRightTop},
      {5018,ExtraPartType.SkirtRight},
      {5019,ExtraPartType.QuarterBackFrontRightBottom},
      {5020,ExtraPartType.QuarterBackFrontRightTop},
      {5021,ExtraPartType.QuarterBackBackRightBottom},
      {5022,ExtraPartType.QuarterBackBackRightTop},
      {5023,ExtraPartType.RearBumperRight}
    };
    private Dictionary<int, string> slotIDToAttach = new Dictionary<int, string>
    {
      {5000, "0x00032006 5000"},
      {5001, "0x00032007 5001"},
      {5002, "0x00032008 5002"},
      {5003, "0x00032009 5003"},
      {5004, "0x0003200A 5004"},
      {5005, "0x0003200B 5005"},
      {5006, "0x0003200C 5006"},
      {5007, "0x0003200D 5007"},
      {5008, "0x0003200E 5008"},
      {5009, "0x0003200F 5009"},
      {5010, "0x00032010 5010"},
      {5011, "0x00032011 5011"},
      {5012, "0x00032012 5012"},
      {5013, "0x00032013 5013"},
      {5014, "0x00032014 5014"},
      {5015, "0x00032015 5015"},
      {5016, "0x00032016 5016"},
      {5017, "0x00032017 5017"},
      {5018, "0x00032018 5018"},
      {5019, "0x00032019 5019"},
      {5020, "0x0003201A 5020"},
      {5021, "0x0003201B 5021"},
      {5022, "0x0003201C 5022"},
      {5023, "0x0003201D 5023"}
    };

    public Dictionary<ExtraPartType, NamedCfgInducedModel> ExtraPartModels
    {
      get;
      private set;
    } = new Dictionary<ExtraPartType, NamedCfgInducedModel>();
    public Dictionary<ExtraPartType, SnappedModel> SnappedModels
    {
      get;
      private set;
    } = new Dictionary<ExtraPartType, SnappedModel>();
    public float XOffsetFromSymmetry
    {
      get;
      set;
    } = 0.0f;

    private float xOffsetNormalized
    {
      get
      {
        return XOffsetFromSymmetry * 100.0f;
      }
    }

    public ExtraPartManager()
    {
      foreach (var val in Enum.GetValues(typeof(ExtraPartType)).Cast<ExtraPartType>())
      {
        ExtraPartModels[val] = new NamedCfgInducedModel
        {
          BodyLine = SlrrLib.Model.CfgBodyLine.GetZeroBodyLine()
        };
      }
    }

    public void EnforceConstraints()
    {
      for (int i = 0; i != 5; i++)
        constraintRound();
      UpdateModels();
    }
    public void UpdateModels()
    {
      foreach (var model in ExtraPartModels)
        updateModelTransform(model.Value);
    }
    public void ReLoadModels()
    {
      SCXModelFactor scxFact = new SCXModelFactor();
      foreach (var val in Enum.GetValues(typeof(ExtraPartType)).Cast<ExtraPartType>())
      {
        ExtraPartModels[val] = new NamedCfgInducedModel(scxFact.GetModels("ExtraParts" + defaultFileName[val], new Vector3D(), new Vector3D()).First() as NamedScxModel);
        ExtraPartModels[val].BodyLine = SlrrLib.Model.CfgBodyLine.GetZeroBodyLine();
      }
      EnforceConstraints();
    }
    public IEnumerable<string> GetSuffixes()
    {
      return System.IO.Directory.EnumerateDirectories("ExtraParts\\");
    }
    public void FillSnappedModelsWithSuffix(string suffix)
    {
      SnappedModels.Clear();
      if (suffix == null || suffix == "")
        return;
      SCXModelFactor scxFact = new SCXModelFactor();

      foreach (var val in Enum.GetValues(typeof(ExtraPartType)).Cast<ExtraPartType>())
      {
        SnappedModels[val] = new SnappedModel
        {
          ObjectModel = scxFact.GetModels(suffix + defaultFileName[val], new Vector3D(), new Vector3D()).First() as NamedScxModel
        };
        ((SnappedModels[val].ObjectModel.ModelGeom.Material as DiffuseMaterial).Brush as ImageBrush).TileMode = TileMode.FlipXY;
        SnappedModels[val].ModelSnappedTo = ExtraPartModels[val];
      }
    }
    public void UpdateSanps()
    {
      foreach (var snapped in SnappedModels)
        snapped.Value.UpdateSnap(xOffsetNormalized);
    }
    public void SaveExtraParts(string rpkFnam, string vehicleCode, int selectedVehicleTypeID, bool onlySlots = false, bool noScxCopy = false)
    {
      SlrrLib.Model.HighLevel.RpkManager rpkManager = new SlrrLib.Model.HighLevel.RpkManager(rpkFnam, SlrrLib.Model.HighLevel.GameFileManager.GetSLRRRoot(rpkFnam));
      SlrrLib.Model.DynamicRpk rpkDat = new SlrrLib.Model.DynamicRpk(new SlrrLib.Model.BinaryRpk(rpkFnam, true));
      List<SlrrLib.Model.Cfg> cfgsToSave = new List<SlrrLib.Model.Cfg>();
      var carCfg = rpkManager.GetCfgFromTypeID(selectedVehicleTypeID);
      if (addSlotsToChassisCfg(carCfg))
        cfgsToSave.Add(carCfg);
      SlrrLib.Model.MessageLog.AddMessage("Cfg mod done : " + carCfg.CfgFileName);
      if (onlySlots)
      {
        carCfg.Save(false);
        return;
      }

      // copyExtraPartsWithSuffixes
      List<ExtraPartSetCatalogEntry> catalogEntries = new List<ExtraPartSetCatalogEntry>();
      foreach (var suffix in GetSuffixes())
      {
        SlrrLib.Model.MessageLog.AddMessage("Processing suffix: " + suffix + " ...");
        FillSnappedModelsWithSuffix(suffix);
        string TargetDir = rpkFnam.Substring(0, rpkFnam.Length - 4) + "_ExtraParts\\" + vehicleCode + "\\" + System.IO.Path.GetFileNameWithoutExtension(suffix);
        string NoSubDivTarget = TargetDir + "\\NoSubDiv";
        if (!System.IO.Directory.Exists(TargetDir))
          System.IO.Directory.CreateDirectory(TargetDir);
        if (!System.IO.Directory.Exists(NoSubDivTarget))
          System.IO.Directory.CreateDirectory(NoSubDivTarget);
        catalogEntries.Add(new ExtraPartSetCatalogEntry(rpkManager.GetRpkAsScriptRPKRefString() + ":" + "0x" + selectedVehicleTypeID.ToString("X8"), suffix));
        var lastEntry = catalogEntries.Last();
        int slotID = 5000;
        foreach (var val in Enum.GetValues(typeof(ExtraPartType)).Cast<ExtraPartType>())
        {
          string currentScx = SnappedModels[val].ObjectModel.ScxFnam;
          string currentScxNoExt = System.IO.Path.GetFileNameWithoutExtension(currentScx);
          var subExtraPartSCXs = System.IO.Directory.EnumerateFiles(System.IO.Path.GetDirectoryName(currentScx),
                                 currentScxNoExt + "_*").Where(x => x.ToLower().EndsWith("scx")).ToList();
          string targetScx = TargetDir + "\\" + System.IO.Path.GetFileName(currentScx);
          if (!noScxCopy)
          {
            System.IO.File.Copy(currentScx, targetScx, true);
            System.IO.File.Copy(currentScx.Substring(0, currentScx.Length - 3) + "tex", TargetDir + "\\" + currentScxNoExt + ".tex", true);
          }
          string targetNoSubDivScx = NoSubDivTarget + "\\" + System.IO.Path.GetFileName(currentScx);
          string sourceNoSubDivScx = System.IO.Path.GetDirectoryName(currentScx) +
                                     "\\NoSubDiv\\" +
                                     System.IO.Path.GetFileName(currentScx);
          if (!noScxCopy)
          {
            System.IO.File.Copy(sourceNoSubDivScx, targetNoSubDivScx, true);
            System.IO.File.Copy(sourceNoSubDivScx.Substring(0, sourceNoSubDivScx.Length - 3) + "tex", NoSubDivTarget + "\\" + System.IO.Path.GetFileNameWithoutExtension(sourceNoSubDivScx) + ".tex", true);
          }
          ExtraPartRPKEntry toadPart = new ExtraPartRPKEntry();
          int freeID = getFreeRangeTypeID(rpkDat, 4);
          toadPart.GenerateSections(targetScx, targetNoSubDivScx, currentScxNoExt, freeID, slotIDToAttach[slotID], subExtraPartSCXs.Count);
          cfgsToSave.Add(toadPart.PartCfg);
          rpkDat.Entries.Add(toadPart.ScxEntry);
          rpkDat.Entries.Add(toadPart.ShapeScxEntry);
          rpkDat.Entries.Add(toadPart.MeshEntry);
          rpkDat.Entries.Add(toadPart.PartResEntry);
          lastEntry.CorrespondingPartResIDs.Add(rpkManager.GetRpkAsScriptRPKRefString() + ":0x" + toadPart.PartResEntry.TypeID.ToString("X8"));

          // add sub extra parts
          int sub_i = 0;
          foreach (var subScx in subExtraPartSCXs)
          {
            sub_i++; // intentionnaly starting at 1
            targetScx = TargetDir + "\\" + System.IO.Path.GetFileName(subScx);
            if (!noScxCopy)
            {
              System.IO.File.Copy(subScx, targetScx, true);
              System.IO.File.Copy(subScx.Substring(0, subScx.Length - 3) + "tex", TargetDir + "\\" + System.IO.Path.GetFileNameWithoutExtension(subScx) + ".tex", true);
            }
            targetNoSubDivScx = NoSubDivTarget + "\\" + System.IO.Path.GetFileName(subScx);
            sourceNoSubDivScx = System.IO.Path.GetDirectoryName(subScx) +
                                "\\NoSubDiv\\" +
                                System.IO.Path.GetFileName(subScx);
            if (!noScxCopy)
            {
              System.IO.File.Copy(sourceNoSubDivScx, targetNoSubDivScx, true);
              System.IO.File.Copy(sourceNoSubDivScx.Substring(0, sourceNoSubDivScx.Length - 3) + "tex", NoSubDivTarget + "\\" + System.IO.Path.GetFileNameWithoutExtension(sourceNoSubDivScx) + ".tex", true);
            }
            ExtraPartRPKEntry toadSubPart = new ExtraPartRPKEntry();
            freeID = getFreeRangeTypeID(rpkDat, 4);
            toadSubPart.GenerateSections(targetScx, targetNoSubDivScx,
                                         currentScxNoExt,
                                         freeID, slotIDToAttach[slotID].Substring(0, 10) + " " + sub_i.ToString(), 0);
            cfgsToSave.Add(toadSubPart.PartCfg);
            rpkDat.Entries.Add(toadSubPart.ScxEntry);
            rpkDat.Entries.Add(toadSubPart.ShapeScxEntry);
            rpkDat.Entries.Add(toadSubPart.MeshEntry);
            rpkDat.Entries.Add(toadSubPart.PartResEntry);
            lastEntry.CorrespondingPartResIDs.Add(rpkManager.GetRpkAsScriptRPKRefString() + ":0x" + toadSubPart.PartResEntry.TypeID.ToString("X8"));
          }
          slotID++;
        }
      }
      System.IO.File.WriteAllLines("ExtraPartsCatalogEntries_" + vehicleCode + ".txt", catalogEntries.Select(x => x.SerializedString()));
      rpkDat.SaveAs(rpkFnam, false);
      foreach (var cfg in cfgsToSave)
        cfg.Save(false);
    }
    public void LoadConfig(string fnam)
    {
      var lns = System.IO.File.ReadAllLines(fnam);
      if (lns.Length < 16)
      {
        SlrrLib.Model.MessageLog.AddError("Extra parts config file contains " + lns.Length.ToString() + " lines which should be at least 16");
        return;
      }
      XOffsetFromSymmetry = float.Parse(lns[0]);
      ExtraPartModels[ExtraPartType.FrontBumper].BodyLine.LineY = float.Parse(lns[1]);
      ExtraPartModels[ExtraPartType.FrontBumper].BodyLine.LineZ = float.Parse(lns[2]);
      ExtraPartModels[ExtraPartType.FrontBumperLeft].BodyLine.LineX = float.Parse(lns[3]);
      ExtraPartModels[ExtraPartType.QuarterFrontFrontLeftBottom].BodyLine.LineX = float.Parse(lns[4]);
      ExtraPartModels[ExtraPartType.QuarterFrontFrontLeftBottom].BodyLine.LineZ = float.Parse(lns[5]);
      ExtraPartModels[ExtraPartType.QuarterFrontFrontLeftTop].BodyLine.LineY = float.Parse(lns[6]);
      ExtraPartModels[ExtraPartType.QuarterFrontBackLeftTop].BodyLine.LineZ = float.Parse(lns[7]);
      ExtraPartModels[ExtraPartType.SkirtLeft].BodyLine.LineZ = float.Parse(lns[8]);
      ExtraPartModels[ExtraPartType.SkirtLeft].BodyLine.LineX = float.Parse(lns[9]);
      ExtraPartModels[ExtraPartType.QuarterBackFrontLeftBottom].BodyLine.LineX = float.Parse(lns[10]);
      ExtraPartModels[ExtraPartType.QuarterBackFrontLeftBottom].BodyLine.LineZ = float.Parse(lns[11]);
      ExtraPartModels[ExtraPartType.QuarterBackFrontLeftTop].BodyLine.LineY = float.Parse(lns[12]);
      ExtraPartModels[ExtraPartType.QuarterBackBackLeftTop].BodyLine.LineZ = float.Parse(lns[13]);
      ExtraPartModels[ExtraPartType.RearBumperLeft].BodyLine.LineZ = float.Parse(lns[14]);
      ExtraPartModels[ExtraPartType.RearBumperLeft].BodyLine.LineX = float.Parse(lns[15]);
      EnforceConstraints();
      UpdateSanps();
    }

    private bool addSlotsToChassisCfg(SlrrLib.Model.Cfg chassisCfg)
    {
      bool wasCarCfgChange = false;
      for (int slot_i = 5000; slot_i != 5024; slot_i++)
      {
        if (!chassisCfg.Slots.Any(x => x.SlotID == slot_i))
        {
          string slotX = (ExtraPartModels[slotIDToExtraPartType[slot_i]].BodyLine.LineX + XOffsetFromSymmetry).ToString("F3");
          string slotY = ExtraPartModels[slotIDToExtraPartType[slot_i]].BodyLine.LineY.ToString("F3");
          string slotZ = ExtraPartModels[slotIDToExtraPartType[slot_i]].BodyLine.LineZ.ToString("F3");
          SlrrLib.Model.CfgSlotLine toad =
            new SlrrLib.Model.CfgSlotLine("slot		" + slotX + " " + slotY + " " + slotZ + " 0.000 0.000 0.000 " + slot_i.ToString() + " ; " + slotIDToExtraPartType[slot_i].ToString()
                                          + "\nattach " + slotIDToAttach[slot_i]);
          chassisCfg.AddSlot(toad);
          wasCarCfgChange = true;
        }
        else
        {
          var slotLine = chassisCfg.Slots.First(x => x.SlotID == slot_i);
          slotLine.LineX = ExtraPartModels[slotIDToExtraPartType[slot_i]].BodyLine.LineX + XOffsetFromSymmetry;
          slotLine.LineY = ExtraPartModels[slotIDToExtraPartType[slot_i]].BodyLine.LineY;
          slotLine.LineZ = ExtraPartModels[slotIDToExtraPartType[slot_i]].BodyLine.LineZ;
          wasCarCfgChange = true;
        }
      }
      return wasCarCfgChange;
    }
    private void constraintRound()
    {
      if (ExtraPartModels.Any(x => x.Value.ModelGeom == null))
        return;
      ExtraPartModels[ExtraPartType.FrontBumper].BodyLine.LineX = 0;

      ExtraPartModels[ExtraPartType.FrontBumperLeft].BodyLine.LineY = ExtraPartModels[ExtraPartType.FrontBumper].BodyLine.LineY;
      ExtraPartModels[ExtraPartType.FrontBumperLeft].BodyLine.LineZ = ExtraPartModels[ExtraPartType.FrontBumper].BodyLine.LineZ;
      ExtraPartModels[ExtraPartType.FrontBumperRight].BodyLine.LineY = ExtraPartModels[ExtraPartType.FrontBumper].BodyLine.LineY;
      ExtraPartModels[ExtraPartType.FrontBumperRight].BodyLine.LineZ = ExtraPartModels[ExtraPartType.FrontBumper].BodyLine.LineZ;
      ExtraPartModels[ExtraPartType.FrontBumperRight].BodyLine.LineX = -ExtraPartModels[ExtraPartType.FrontBumperLeft].BodyLine.LineX;

      ExtraPartModels[ExtraPartType.QuarterFrontFrontLeftBottom].BodyLine.LineY = ExtraPartModels[ExtraPartType.FrontBumperLeft].BodyLine.LineY;
      ExtraPartModels[ExtraPartType.QuarterFrontFrontRightBottom].BodyLine.LineX = -ExtraPartModels[ExtraPartType.QuarterFrontFrontLeftBottom].BodyLine.LineX;
      ExtraPartModels[ExtraPartType.QuarterFrontFrontRightBottom].BodyLine.LineY = ExtraPartModels[ExtraPartType.FrontBumperRight].BodyLine.LineY;
      ExtraPartModels[ExtraPartType.QuarterFrontFrontRightBottom].BodyLine.LineZ = ExtraPartModels[ExtraPartType.QuarterFrontFrontLeftBottom].BodyLine.LineZ;

      ExtraPartModels[ExtraPartType.QuarterFrontFrontLeftTop].BodyLine.LineX = ExtraPartModels[ExtraPartType.QuarterFrontFrontLeftBottom].BodyLine.LineX;
      ExtraPartModels[ExtraPartType.QuarterFrontFrontLeftTop].BodyLine.LineZ = ExtraPartModels[ExtraPartType.QuarterFrontFrontLeftBottom].BodyLine.LineZ;
      ExtraPartModels[ExtraPartType.QuarterFrontFrontRightTop].BodyLine.LineX = ExtraPartModels[ExtraPartType.QuarterFrontFrontRightBottom].BodyLine.LineX;
      ExtraPartModels[ExtraPartType.QuarterFrontFrontRightTop].BodyLine.LineZ = ExtraPartModels[ExtraPartType.QuarterFrontFrontRightBottom].BodyLine.LineZ;
      ExtraPartModels[ExtraPartType.QuarterFrontFrontRightTop].BodyLine.LineY = ExtraPartModels[ExtraPartType.QuarterFrontFrontLeftTop].BodyLine.LineY;

      ExtraPartModels[ExtraPartType.QuarterFrontBackLeftTop].BodyLine.LineX = ExtraPartModels[ExtraPartType.QuarterFrontFrontLeftTop].BodyLine.LineX;
      ExtraPartModels[ExtraPartType.QuarterFrontBackLeftTop].BodyLine.LineY = ExtraPartModels[ExtraPartType.QuarterFrontFrontLeftTop].BodyLine.LineY;
      ExtraPartModels[ExtraPartType.QuarterFrontBackRightTop].BodyLine.LineX = ExtraPartModels[ExtraPartType.QuarterFrontFrontRightTop].BodyLine.LineX;
      ExtraPartModels[ExtraPartType.QuarterFrontBackRightTop].BodyLine.LineY = ExtraPartModels[ExtraPartType.QuarterFrontFrontRightTop].BodyLine.LineY;
      ExtraPartModels[ExtraPartType.QuarterFrontBackRightTop].BodyLine.LineZ = ExtraPartModels[ExtraPartType.QuarterFrontBackLeftTop].BodyLine.LineZ;

      ExtraPartModels[ExtraPartType.QuarterFrontBackLeftBottom].BodyLine.LineZ = ExtraPartModels[ExtraPartType.QuarterFrontBackLeftTop].BodyLine.LineZ;
      ExtraPartModels[ExtraPartType.QuarterFrontBackLeftBottom].BodyLine.LineX = ExtraPartModels[ExtraPartType.QuarterFrontBackLeftTop].BodyLine.LineX;
      ExtraPartModels[ExtraPartType.QuarterFrontBackLeftBottom].BodyLine.LineY = ExtraPartModels[ExtraPartType.QuarterFrontFrontLeftBottom].BodyLine.LineY;
      ExtraPartModels[ExtraPartType.QuarterFrontBackRightBottom].BodyLine.LineZ = ExtraPartModels[ExtraPartType.QuarterFrontBackRightTop].BodyLine.LineZ;
      ExtraPartModels[ExtraPartType.QuarterFrontBackRightBottom].BodyLine.LineX = ExtraPartModels[ExtraPartType.QuarterFrontBackRightTop].BodyLine.LineX;
      ExtraPartModels[ExtraPartType.QuarterFrontBackRightBottom].BodyLine.LineY = ExtraPartModels[ExtraPartType.QuarterFrontBackLeftBottom].BodyLine.LineY;

      ExtraPartModels[ExtraPartType.SkirtLeft].BodyLine.LineY = ExtraPartModels[ExtraPartType.QuarterFrontBackLeftBottom].BodyLine.LineY;
      ExtraPartModels[ExtraPartType.SkirtRight].BodyLine.LineX = -ExtraPartModels[ExtraPartType.SkirtLeft].BodyLine.LineX;
      ExtraPartModels[ExtraPartType.SkirtRight].BodyLine.LineY = ExtraPartModels[ExtraPartType.QuarterFrontBackRightBottom].BodyLine.LineY;
      ExtraPartModels[ExtraPartType.SkirtRight].BodyLine.LineZ = ExtraPartModels[ExtraPartType.SkirtLeft].BodyLine.LineZ;

      ExtraPartModels[ExtraPartType.QuarterBackFrontLeftBottom].BodyLine.LineY = ExtraPartModels[ExtraPartType.SkirtLeft].BodyLine.LineY;
      ExtraPartModels[ExtraPartType.QuarterBackFrontRightBottom].BodyLine.LineY = ExtraPartModels[ExtraPartType.SkirtRight].BodyLine.LineY;
      ExtraPartModels[ExtraPartType.QuarterBackFrontRightBottom].BodyLine.LineX = -ExtraPartModels[ExtraPartType.QuarterBackFrontLeftBottom].BodyLine.LineX;
      ExtraPartModels[ExtraPartType.QuarterBackFrontRightBottom].BodyLine.LineZ = ExtraPartModels[ExtraPartType.QuarterBackFrontLeftBottom].BodyLine.LineZ;

      ExtraPartModels[ExtraPartType.QuarterBackFrontLeftTop].BodyLine.LineX = ExtraPartModels[ExtraPartType.QuarterBackFrontLeftBottom].BodyLine.LineX;
      ExtraPartModels[ExtraPartType.QuarterBackFrontLeftTop].BodyLine.LineZ = ExtraPartModels[ExtraPartType.QuarterBackFrontLeftBottom].BodyLine.LineZ;
      ExtraPartModels[ExtraPartType.QuarterBackFrontRightTop].BodyLine.LineX = ExtraPartModels[ExtraPartType.QuarterBackFrontRightBottom].BodyLine.LineX;
      ExtraPartModels[ExtraPartType.QuarterBackFrontRightTop].BodyLine.LineZ = ExtraPartModels[ExtraPartType.QuarterBackFrontRightBottom].BodyLine.LineZ;
      ExtraPartModels[ExtraPartType.QuarterBackFrontRightTop].BodyLine.LineY = ExtraPartModels[ExtraPartType.QuarterBackFrontLeftTop].BodyLine.LineY;

      ExtraPartModels[ExtraPartType.QuarterBackBackLeftTop].BodyLine.LineX = ExtraPartModels[ExtraPartType.QuarterBackFrontLeftTop].BodyLine.LineX;
      ExtraPartModels[ExtraPartType.QuarterBackBackLeftTop].BodyLine.LineY = ExtraPartModels[ExtraPartType.QuarterBackFrontLeftTop].BodyLine.LineY;
      ExtraPartModels[ExtraPartType.QuarterBackBackRightTop].BodyLine.LineX = ExtraPartModels[ExtraPartType.QuarterBackFrontRightBottom].BodyLine.LineX;
      ExtraPartModels[ExtraPartType.QuarterBackBackRightTop].BodyLine.LineY = ExtraPartModels[ExtraPartType.QuarterBackBackLeftTop].BodyLine.LineY;
      ExtraPartModels[ExtraPartType.QuarterBackBackRightTop].BodyLine.LineZ = ExtraPartModels[ExtraPartType.QuarterBackBackLeftTop].BodyLine.LineZ;

      ExtraPartModels[ExtraPartType.QuarterBackBackLeftBottom].BodyLine.LineZ = ExtraPartModels[ExtraPartType.QuarterBackBackLeftTop].BodyLine.LineZ;
      ExtraPartModels[ExtraPartType.QuarterBackBackLeftBottom].BodyLine.LineX = ExtraPartModels[ExtraPartType.QuarterBackBackLeftTop].BodyLine.LineX;
      ExtraPartModels[ExtraPartType.QuarterBackBackLeftBottom].BodyLine.LineY = ExtraPartModels[ExtraPartType.QuarterBackFrontLeftBottom].BodyLine.LineY;
      ExtraPartModels[ExtraPartType.QuarterBackBackRightBottom].BodyLine.LineZ = ExtraPartModels[ExtraPartType.QuarterBackBackRightTop].BodyLine.LineZ;
      ExtraPartModels[ExtraPartType.QuarterBackBackRightBottom].BodyLine.LineX = ExtraPartModels[ExtraPartType.QuarterBackBackRightTop].BodyLine.LineX;
      ExtraPartModels[ExtraPartType.QuarterBackBackRightBottom].BodyLine.LineY = ExtraPartModels[ExtraPartType.QuarterBackBackLeftBottom].BodyLine.LineY;

      ExtraPartModels[ExtraPartType.RearBumperLeft].BodyLine.LineY = ExtraPartModels[ExtraPartType.FrontBumperLeft].BodyLine.LineY;
      ExtraPartModels[ExtraPartType.RearBumperRight].BodyLine.LineY = ExtraPartModels[ExtraPartType.RearBumperLeft].BodyLine.LineY;
      ExtraPartModels[ExtraPartType.RearBumperRight].BodyLine.LineZ = ExtraPartModels[ExtraPartType.RearBumperLeft].BodyLine.LineZ;
      ExtraPartModels[ExtraPartType.RearBumperRight].BodyLine.LineX = -ExtraPartModels[ExtraPartType.RearBumperLeft].BodyLine.LineX;

      ExtraPartModels[ExtraPartType.RearBumper].BodyLine.LineY = ExtraPartModels[ExtraPartType.RearBumperLeft].BodyLine.LineY;
      ExtraPartModels[ExtraPartType.RearBumper].BodyLine.LineZ = ExtraPartModels[ExtraPartType.RearBumperLeft].BodyLine.LineZ;
      ExtraPartModels[ExtraPartType.RearBumper].BodyLine.LineX = 0;
    }
    private void updateModelTransform(NamedCfgInducedModel model)
    {
      if (model.ModelGeom == null)
        return;
      Transform3DGroup transform3DGroup = new Transform3DGroup();
      transform3DGroup.Children.Add(new TranslateTransform3D(NamedCfgInducedModel.GetTranslateVec3FromBodyLinePos(model.BodyLine)
                                    + new Vector3D(xOffsetNormalized, 0, 0)));
      model.ModelGeom.Transform = transform3DGroup;
    }
    private int getFreeRangeTypeID(SlrrLib.Model.DynamicRpk rpkDat, int rangeLength = 1, int minID = 0x00001000)
    {
      var ordered = rpkDat.Entries.Select(x => x.TypeID).OrderBy(x => x).ToList();
      for (int i = 1; i < ordered.Count; ++i)
      {
        if (ordered[i] - ordered[i - 1] > rangeLength && ordered[i - 1] > minID)
        {
          if (ordered[i - 1] + rangeLength > 0x0000FFFF)
            throw new Exception("Local TypeID range exceded can not insert any more entries in rpk!");
          return ordered[i - 1] + 1;
        }
        if (ordered[i - 1] < minID && ordered[i] > minID && ordered[i] - minID > rangeLength)
        {
          if (minID + rangeLength > 0x0000FFFF)
            throw new Exception("Local TypeID range exceded can not insert any more entries in rpk!");
          return minID;
        }
      }
      int fallBackTarget = ordered.Last() + 1;
      if (fallBackTarget < minID)
        fallBackTarget = minID;
      if (fallBackTarget + rangeLength > 0x0000FFFF)
        throw new Exception("Local TypeID range exceded can not insert any more entries in rpk!");
      return fallBackTarget;
    }
  }
}
