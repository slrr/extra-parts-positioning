﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraPartsPositioning.Model
{
  internal class CfgPosLineView
  {
    private SlrrLib.Model.CfgSlotLine slotLine;
    public SlrrLib.Model.CfgPartPosRotLine Line
    {
      get;
      private set;
    }
    public override string ToString()
    {
      if (slotLine.PosLine.Tokens.Any(x => x.IsComment))
      {
        return slotLine.PosLine.Tokens.First(x => x.IsComment).Value;
      }
      return slotLine.ToString();
    }

    public CfgPosLineView(SlrrLib.Model.CfgPartPosRotLine line, SlrrLib.Model.CfgSlotLine slotline)
    {
      slotLine = slotline;
      Line = line;
    }
  }
}
