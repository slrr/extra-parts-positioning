﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraPartsPositioning.Model
{
  public class ExtraPartSetCatalogEntry
  {
    private string carResID = "";
    private string extraPartName = "";

    public List<string> CorrespondingPartResIDs
    {
      get;
      private set;
    } = new List<string>();

    public ExtraPartSetCatalogEntry(string carResID,string extraPartName)
    {
      this.carResID = carResID;
      this.extraPartName = extraPartName;
    }
    public string SerializedString()
    {
      StringBuilder sb = new StringBuilder();
      sb.AppendLine("\t\ttoad = new ExtraPartCatalogEntry();");
      sb.AppendLine("\t\ttoad.CompatibleCarTypeID = " + carResID + "r;");
      sb.AppendLine("\t\ttoad.PackName = \"" + extraPartName.Replace("ExtraParts\\", "").Replace('_', ' ') + "\";");
      sb.AppendLine("\t\ttoad.PartTypeIDs = new int[" + CorrespondingPartResIDs.Count.ToString() + "];");
      for (int i = 0; i != CorrespondingPartResIDs.Count; ++i)
      {
        sb.AppendLine("\t\ttoad.PartTypeIDs[" + i.ToString() + "] = " + CorrespondingPartResIDs[i] + "r;");
      }
      sb.AppendLine("\t\tExtraPartsPacks.addElement(toad);");
      return sb.ToString();
    }
  }
}
