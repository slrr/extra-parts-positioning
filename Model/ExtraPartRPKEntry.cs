﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraPartsPositioning.Model
{
  public class ExtraPartRPKEntry
  {
    private Dictionary<string, int> textNameToExternalRef = new Dictionary<string, int>
    {
      {"carbon.png",0x00032000},
      {"chrome.png",0x00032001},
      {"env_map.png",0x00032002},
      {"glass.png",0x00032003},
      {"grill.png",0x00032004},
      {"invisible.png",0x00032025},
      {"paintable.png",0x00032005}
    };
    private string extraPartScript = @"parts\scripts\ExtraPart.class";

    public SlrrLib.Model.DynamicResEntry ScxEntry
    {
      get;
      private set;
    }
    public SlrrLib.Model.DynamicResEntry ShapeScxEntry
    {
      get;
      private set;
    }
    public SlrrLib.Model.DynamicResEntry MeshEntry
    {
      get;
      private set;
    }
    public SlrrLib.Model.DynamicResEntry PartResEntry
    {
      get;
      private set;
    }
    public SlrrLib.Model.Cfg PartCfg
    {
      get;
      private set;
    }

    public void GenerateSections(string scxFnam, string noSubDivScxFnam, string physScxFnam, int firstFreeTypeID, string compatibleSlotTypeIDAndID, int numOfSubExtras)
    {
      string modelName = System.IO.Path.GetFileNameWithoutExtension(scxFnam);
      string physFnam = @"cars\meshes\ExtraParts\phys\" + physScxFnam + ".scx";
      int typeID = firstFreeTypeID;
      string slrrRoot = SlrrLib.Model.HighLevel.GameFileManager.GetSLRRRoot(System.IO.Path.GetFullPath(scxFnam)).ToLower() + "\\";
      ScxEntry = new SlrrLib.Model.DynamicResEntry
      {
        TypeID = typeID,
        TypeOfEntry = 9,// click
        SuperID = 0x00000000,
        IsParentCompatible = 1.0f,
        Alias = modelName,
        RSD = new SlrrLib.Model.DynamicRsdEntry()
      };
      ScxEntry.RSD.InnerEntries.Add(new SlrrLib.Model.DynamicStringInnerEntry("shape " + System.IO.Path.GetFullPath(noSubDivScxFnam).Remove(0, slrrRoot.Length)));
      typeID++;
      ShapeScxEntry = new SlrrLib.Model.DynamicResEntry
      {
        TypeID = typeID,
        TypeOfEntry = 5,// mesh
        SuperID = 0x00000000,
        IsParentCompatible = 1.0f,
        Alias = modelName,
        RSD = new SlrrLib.Model.DynamicRsdEntry()
      };
      ShapeScxEntry.RSD.InnerEntries.Add(new SlrrLib.Model.DynamicStringInnerEntry("sourcefile " + System.IO.Path.GetFullPath(scxFnam).Remove(0, slrrRoot.Length)));
      typeID++;
      var texStr = new string[] { "paintable.png" };
      if (System.IO.File.Exists(scxFnam.Substring(0, scxFnam.Length - 3) + "tex"))
      {
        texStr = System.IO.File.ReadAllLines(scxFnam.Substring(0, scxFnam.Length - 3) + "tex");
      }
      for (int tex_i = 0; tex_i != texStr.Length; ++tex_i)
      {
        texStr[tex_i] = "0x" + textNameToExternalRef[texStr[tex_i]].ToString("X8");
      }
      MeshEntry = new SlrrLib.Model.DynamicResEntry
      {
        TypeID = typeID,
        TypeOfEntry = 14,// render
        SuperID = 0x00000000,
        IsParentCompatible = 1.0f,
        Alias = modelName,
        RSD = new SlrrLib.Model.DynamicRsdEntry()
      };
      string texturePart = "";
      if (texStr.Any())
      {
        texturePart = texStr.Select(x => "texture " + x).Aggregate((x, y) => x + "\r\n" + y);
      }
      MeshEntry.RSD.InnerEntries.Add(
        new SlrrLib.Model.DynamicStringInnerEntry(
          "mesh 0x" + ShapeScxEntry.TypeID.ToString("X8") +
          "\r\nflags 8.000\r\n" + texturePart));
      typeID++;
      string subExtrasSlots = "";
      for (int sub_i = 0; sub_i != numOfSubExtras; sub_i++)
      {
        subExtrasSlots += "slot		       0.000 0.000 0.000  0.000 0.000 0.000   " + (sub_i + 2).ToString()
                          + "\r\nattach     " + compatibleSlotTypeIDAndID.Substring(0, 10) + " " + (sub_i + 1).ToString() + "\r\n";
      }
      string cfgModel = "render    0x" + MeshEntry.TypeID.ToString("X8") +
                        "\r\nmesh      0x" + ShapeScxEntry.TypeID.ToString("X8") +
                        "\r\ntexture   0x00032005\r\nclick     0x" + ScxEntry.TypeID.ToString("X8") +
                        "\r\n\r\nbody		0.000 0.000 0.000	0.000 0.000 0.000	0.0001	" + physFnam +
                        "\r\nnoclick\r\n\r\ncategory	2\r\n\r\nslot		       0.000 0.000 0.000  0.000 0.000 0.000   1\r\ncompatible     "
                        + compatibleSlotTypeIDAndID + "\r\n" + subExtrasSlots + "\r\neof\r\n";
      PartCfg = new SlrrLib.Model.Cfg(scxFnam.Substring(0, scxFnam.Length - 3) + "cfg", cfgModel);
      PartResEntry = new SlrrLib.Model.DynamicResEntry
      {
        TypeID = typeID,
        TypeOfEntry = 8,// part
        SuperID = 0x00000000,// replacement parts superID is 0x0004F24F
        IsParentCompatible = 1.0f,
        Alias = modelName,
        RSD = new SlrrLib.Model.DynamicRsdEntry()
      };
      PartResEntry.RSD.InnerEntries.Add(new SlrrLib.Model.DynamicStringInnerEntry("script " + extraPartScript + "\r\n" +
                                        "native part  " + System.IO.Path.GetFullPath(PartCfg.CfgFileName).Remove(0, slrrRoot.Length)));
      typeID++;
    }
  }
}
