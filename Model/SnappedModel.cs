﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using SlrrLib.Geom;

namespace ExtraPartsPositioning.Model
{
  public class SnappedModel
  {
    public NamedScxModel ObjectModel
    {
      get;
      set;
    }
    public NamedCfgInducedModel ModelSnappedTo
    {
      get;
      set;
    }
    public void UpdateSnap(float XOffsetNormalized)
    {
      Transform3DGroup transform3DGroup = new Transform3DGroup();
      transform3DGroup.Children.Add(new TranslateTransform3D(NamedCfgInducedModel.GetTranslateVec3FromBodyLinePos(ModelSnappedTo.BodyLine)
                                    + new Vector3D(XOffsetNormalized, 0, 0)));
      ObjectModel.ModelGeom.Transform = transform3DGroup;
    }
  }
}
